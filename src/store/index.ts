import { InjectionKey } from 'vue';
import { createStore, useStore as marvaelosStore, Store } from 'vuex';
import { State } from './types/state';
import { getMarvelEntities } from '../api/methods/getCharacters';
import { Character } from '../api/types/mavel.model';

export const key: InjectionKey<Store<State>> = Symbol();

export const store = createStore<State>({
	state: {
		characters: [] as Character[] | undefined,
		collectionSize: 0,
		currentPage: 1,
	},
	mutations: {
		setState(state, { characters, total }) {
			state.characters = characters;
			state.collectionSize = total;
		},
		setCurrentPage(state, page) {
			state.currentPage = page;
		},
	},
	actions: {
		async setState({ commit }, offset = 0) {
			const { results, total } = (await getMarvelEntities(offset))!;
			commit('setState', { characters: results, total });
		},
		async setCurrentPage({ commit, dispatch }, page) {
			commit('setCurrentPage', page);
		},
	},
});

// define your own `useStore` composition function
export function useStore() {
	return marvaelosStore(key);
}
