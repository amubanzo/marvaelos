import { Character } from '../../api/types/mavel.model';

export interface State {
	characters: Character[] | undefined;
	collectionSize: number;
	currentPage: number | string;
}