import { Character, MarvelEntity } from '../types/mavel.model';
import apiClient from '../index';

export const getMarvelEntities = async (
	offset: number
): Promise<MarvelEntity | undefined> => {
	try {
		return (
			await apiClient.get<Character[] | undefined>(`characters`, {
				params: {
					offset,
				},
			})
		).data as MarvelEntity;
	} catch (e: any) {
		throw new Error(e.message);
	}
};
