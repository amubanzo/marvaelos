import { AxiosResponseTransformer } from 'axios';
import { CharacterDataWrapper, MarvelEntity } from './types/mavel.model';

export const transformer: AxiosResponseTransformer = (
	response,
	headers
): MarvelEntity => {
	const { status, code, data } = JSON.parse(response) as CharacterDataWrapper;
	return { ...data, status, code };
};
