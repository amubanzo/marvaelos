import axios, { AxiosInstance } from 'axios';
import { md } from 'node-forge';
import { transformer } from './transform';

const { pvKey, apikey, ts } = {
	pvKey: `dc9a7aac78f480651e9fdd4567d3299c44cc67f6`,
	apikey: `1c17043d9e9f2c7c422c8b08eeff9a8c`,
	ts: `${Date.now()}`,
};

const apiClient: AxiosInstance = axios.create({
	baseURL: 'https://gateway.marvel.com/v1/public/',
	headers: {
		'Content-type': 'application/json',
		Accept: '*/*',
	},
	transformResponse: transformer,
	method: 'GET',
	params: {
		apikey,
		ts,
		hash: `${md.md5
			.create()
			.update(`${ts}${pvKey}${apikey}`)
			.digest()
			.toHex()}`,
		limit: 20,
	},
});

export default apiClient;
